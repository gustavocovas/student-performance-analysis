Analysis of student performance dataset
===

Download dataset:
```
wget https://archive.ics.uci.edu/ml/machine-learning-databases/00320/student.zip
unzip student.zip
```

Install python libraries:
```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Run jupyter:
```
jupyter notebook
```
